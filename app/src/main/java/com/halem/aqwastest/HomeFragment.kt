package com.halem.aqwastest


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.transition.TransitionInflater
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.halem.aqwastest.databinding.FragmentHomeBinding
import com.halem.taskksa.network.Api
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.HttpException

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=FragmentHomeBinding.inflate(inflater)
        binding.anotherSuggest.setOnClickListener {
            if ((activity as MapsActivity).mLastKnownLocation!=null)
            (activity as MapsActivity).getResturantServer(
                LatLng(
                    (activity as MapsActivity).mLastKnownLocation!!.latitude,
                    (activity as MapsActivity).mLastKnownLocation!!.longitude
                )
            )
        }

        binding.upButton.setOnClickListener {
            ViewAnimationUtils.collapse(binding.sheet)
        }

        binding.titleToolbar.setOnClickListener{
            tranisition()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if ((activity as MapsActivity).mLastKnownLocation!=null)
                (activity as MapsActivity).getResturantServer(
            LatLng(
                (activity as MapsActivity).mLastKnownLocation!!.latitude,
                (activity as MapsActivity).mLastKnownLocation!!.longitude
            )
        )
    }

    fun tranisition(){
        val fragmentSplash = SplashFragment()
//        sharedElementReturnTransition = TransitionInflater.from(activity).inflateTransition(R.transition.transform)
//        exitTransition = TransitionInflater.from(activity).inflateTransition(android.R.transition.move)
//            .addSharedElement(binding.logo,getString(R.string.logoIcon))

        fragmentSplash.sharedElementEnterTransition = TransitionInflater.from(activity).inflateTransition(R.transition.transform)
        fragmentSplash.enterTransition = TransitionInflater.from(activity).inflateTransition(android.R.transition.fade)


        val transaction =activity!!.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_down,0)
        transaction.addSharedElement(binding.iconLogo,getString(R.string.logoIcon))
        transaction.addSharedElement(binding.titleToolbar,getString(R.string.titleApp))
        transaction.addSharedElement(binding.anotherSuggest,getString(R.string.buttonSugget))
        transaction.replace(R.id.map,fragmentSplash,"home").commit()



    }


    fun updateMapsWithNewLocation(result: RestModel) {
        binding.data = result
        (activity as MapsActivity).mMap.clear()
        (activity as MapsActivity).mMap.addMarker(
            MarkerOptions().position(LatLng(result.lat.toDouble(), result.lon.toDouble()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker))
        )
        (activity as MapsActivity).mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    result.lat.toDouble(),
                    result.lon.toDouble()
                ), 14f
            )
        )
        ViewAnimationUtils.expand(binding.sheet)
        binding.details.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(result.link))
            startActivity(browserIntent)
        }
        binding.googlemaps.setOnClickListener {

            val gmmIntentUri = Uri.parse("https://www.google.com/maps/search/?api=1&query=${result.lat},${result.lon}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            if (mapIntent.resolveActivity(context!!.packageManager) != null) {
                startActivity(mapIntent)
            }
        }

    }
}
