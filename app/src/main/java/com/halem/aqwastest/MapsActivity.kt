package com.halem.aqwastest

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.halem.aqwastest.databinding.ActivityMapsBinding
import com.halem.taskksa.network.Api
import kotlinx.coroutines.*
import retrofit2.HttpException

open class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 12
    private var mLocationPermissionGranted = false
    var mLastKnownLocation: Location? = null
    private var DEFAULT_ZOOM = 14f
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.activity_maps
        )
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        supportFragmentManager.beginTransaction().add(R.id.map,SplashFragment()).commit()
    }




     override fun onMapReady(googleMap: GoogleMap) {
            mMap = googleMap
            // Do other setup activities here too, as described elsewhere in this tutorial.
            googleMap.setOnMarkerClickListener(this)
            // Turn on the My Location layer and the related control on the map.
            updateLocationUI()

            // Get the current location of the device and set the position of the map.
            getDeviceLocation()
    }

    private fun getLocationPermission() {
        /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

     override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    private fun updateLocationUI() {
        if (mMap == null) {
            return
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }


    private fun getDeviceLocation() {
        /*
     * Get the best and most recent location of the device, which may be null in rare
     * cases when a location is not available.
     */
        try {
            if (mLocationPermissionGranted) {
                val mFusedLocationProviderClient = FusedLocationProviderClient(this)
                val locationResult = mFusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(
                    this@MapsActivity
                ) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.result

                        Log.v("mLastKnownLocation",mLastKnownLocation.toString())
                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    mLastKnownLocation!!.latitude,
                                    mLastKnownLocation!!.longitude
                                ), DEFAULT_ZOOM
                            )
                        )
                    } else {
                        Log.d("Maps", "Current location is null. Using defaults.")
                        Log.e("Maps", "Exception: %s", task.exception)
                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    0.0,
                                    0.0
                                ),
                                DEFAULT_ZOOM
                            )
                        )
                        Toast.makeText(this, "لا يمكن الحصول علي الموقع", Toast.LENGTH_SHORT).show()
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

    fun getResturantServer(location: LatLng) {
//        Log.v("here", "${location.latitude},${location.longitude}")
//        showLoading.value=true
        val viewModelJob = SupervisorJob()
        val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )
        coroutineScope.launch {

            val response = Api.retrofitService.getRestaurant("${location.latitude},${location.longitude}")
            try {
                val result = response.await()
                val homeFragment : HomeFragment=supportFragmentManager.findFragmentByTag("home") as HomeFragment

                if (homeFragment.isAdded)
                    homeFragment.updateMapsWithNewLocation(result)
//                Toast.makeText(this@MapsActivity,result.success, Toast.LENGTH_LONG).show()
            } catch (e: Exception) {
                Toast.makeText(this@MapsActivity,getString(R.string.errorNet), Toast.LENGTH_LONG).show()
                Log.v("errorSubmitApi", e.toString())
//                Log.v("errorSubmitResponse", e.response().errorBody()!!.string().toString())


//                dataCities.value = ArrayList()
            }
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        val homeFragment : HomeFragment=supportFragmentManager.findFragmentByTag("home") as HomeFragment
        if (homeFragment.isAdded)
        ViewAnimationUtils.expand(homeFragment.binding.sheet)
        return false
    }
}


