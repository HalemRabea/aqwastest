package com.halem.aqwastest


import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.google.android.gms.maps.model.LatLng
import com.halem.aqwastest.databinding.FragmentSplashBinding
import kotlinx.android.synthetic.main.activity_main.*

/**
 * A simple [Fragment] subclass.
 */
class SplashFragment : Fragment() {
lateinit var binding:FragmentSplashBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=FragmentSplashBinding.inflate(inflater)

        binding.suggest.setOnClickListener {
//            goToMaps
            tranisition()


    }


        animateFirstViews()
        return binding.root
    }
    fun animateFirstViews(){
        val upLogo = AnimationUtils.loadAnimation(activity, R.anim.up_logo)
        val upTitle = AnimationUtils.loadAnimation(activity, R.anim.up_title)
        val upSuggest = AnimationUtils.loadAnimation(activity, R.anim.up_suggest)
        binding.logo.startAnimation(upLogo)
        binding.titleApp.startAnimation(upTitle)
        binding.suggest.startAnimation(upSuggest)

    }

    fun tranisition(){
        val fragmentHome = HomeFragment()
//        sharedElementReturnTransition = TransitionInflater.from(activity).inflateTransition(R.transition.transform)
//        exitTransition = TransitionInflater.from(activity).inflateTransition(android.R.transition.move)
//            .addSharedElement(binding.logo,getString(R.string.logoIcon))

        fragmentHome.sharedElementEnterTransition = TransitionInflater.from(activity).inflateTransition(R.transition.transform)
        fragmentHome.enterTransition = TransitionInflater.from(activity).inflateTransition(android.R.transition.fade)


        val transaction =activity!!.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.hold,R.anim.slide_up,R.anim.slide_down,R.anim.hold)
        transaction.addSharedElement(binding.logo,getString(R.string.logoIcon))
        transaction.addSharedElement(binding.titleApp,getString(R.string.titleApp))
        transaction.addSharedElement(binding.suggest,getString(R.string.buttonSugget))
        transaction.replace(R.id.map,fragmentHome,"home").commit()


//        (activity as MapsActivity).getResturantServer(
//            LatLng(
//                (activity as MapsActivity).mLastKnownLocation!!.latitude,
//                (activity as MapsActivity).mLastKnownLocation!!.longitude
//            )
//        )
    }

}
