package com.halem.taskksa.network


import com.google.android.gms.maps.model.LatLng
import com.halem.aqwastest.RestModel
import kotlinx.coroutines.Deferred
import retrofit2.http.*


interface ApiService {
    @GET("GenerateFS.php?get_param=value")
    fun getRestaurant(@Query("uid")location: String): Deferred<RestModel>


}