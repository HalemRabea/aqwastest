package com.halem.aqwastest


import com.squareup.moshi.Json

data class RestModel(@Json(name = "image")
                     val image: List<String>?,
                     @Json(name = "Ulat")
                     val ulat: String = "",
                     @Json(name = "link")
                     val link: String = "",
                     @Json(name = "rating")
                     val rating: String = "",
                     @Json(name = "lon")
                     val lon: String = "",
                     @Json(name = "error")
                     val error: String = "",
                     @Json(name = "catId")
                     val catId: String = "",
                     @Json(name = "cat")
                     val cat: String = "",
                     @Json(name = "name")
                     val name: String = "",
                     @Json(name = "id")
                     val id: String = "",
                     @Json(name = "lat")
                     val lat: String = "",
                     @Json(name = "Ulon")
                     val ulon: String = "",
                     @Json(name = "open")
                     val open: String = "")


